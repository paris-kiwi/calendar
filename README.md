# NAME

Paris::Kiwi - DIY scene events calendar

# DESCRIPTION

[Paris::Kiwi](https://pariskiwi.org) is a wiki events agenda for Paris and it's
suburb. This is the second software version, the first one was based on
Mediawiki.

# SYNOPSIS

    my $agenda = Paris::Kiwi::Agenda->new({
      current_year => $c->param('year'),
      current_month => $month
    });

# HELPERS

They can be used in template by being called by their name but are globally accessible from everywhere with `app-`helper\_name>

## db

Give access to the db schema from everywhere.

## standard\_date

Turns idx dates, e.g. `2020-4-1` used by some calendar or datetime toolkits to
proper dates e.g. `2020-04-01`.

# AUTHORS

Sébastien Feugère <sebastien@feugere.net>

# LICENSE

Copyright (C) Sébastien Feugère

This library is free software; you can redistribute it and/or modify it under the same terms as Perl itself.
