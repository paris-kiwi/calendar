cal:
	carton exec -- pcal

run:
	DBIC_TRACE=1 carton exec -- morbo script/paris_kiwi

prod:
	carton exec -- hypnotoad ./script/paris_kiwi

test:
	carton exec -- prove -lv t/

dbic-dump:
	carton exec -- dbicdump -o dump_directory=./lib \
	-o components='["InflateColumn::DateTime"]' \
	Paris::Kiwi::Schema dbi:SQLite:./data/kiwi.db

create-db:
	sqlite3 data/kiwi.db < data/schema.sql

dump-schema:
	sqlite3 data/kiwi.db .schema > data/schema.sql

dump-data:
	sqlite3 data/kiwi.db .dump > data/dump.sql
