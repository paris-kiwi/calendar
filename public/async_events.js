(function() {
  var month = $('table').data('month') ;
  var year = $('table').data('year');
  var route = '/events/' + year + '/' + month;
  $.getJSON( route, {
    format: "json"
  })
    .done(function( data ) {
      $.each( data, function( i, item ) {
        var date = new Date(item.timestamp * 1000);
        var composedDate = date.toISOString().substring(0, 10);
        var selector = '[data-date="' + composedDate + '"]';
        $( '<li><a href="/' + year + '/' + month + '/' + item.url + '"><time></time> ' + item.title + '</a></li>' ).appendTo( $( selector + ' template') );
        $( selector + ' template').show();
      });
    });
})();
