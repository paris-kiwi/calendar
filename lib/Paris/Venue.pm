package Paris::Venue;
use Mojo::Base -base, -signatures;
use Role::Tiny::With;

with 'Paris::Kiwi::Role::Identifier';
with 'Paris::Kiwi::Role::Website';

has 'address';
has 'latitude';
has 'longitude';
# Bar, squat, non-profit, other
has 'kind';


1;
