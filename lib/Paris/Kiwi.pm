package Paris::Kiwi;
use Time::Moment;
use Mojo::Base 'Mojolicious';
use Paris::Kiwi::Schema;

our $VERSION = '0.14';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by config file
  my $config = $self->plugin('Config');

  # Mostly useful when working on CSS
  # $self->plugin('AutoReload');

  has schema => sub {
      my $schema = Paris::Kiwi::Schema
              ->connect( 'dbi:SQLite:./data/kiwi.db',
                         '', '',
                         { sqlite_unicode => 1} );
  };

  $self->helper( db => sub { $self->app->schema } );

  $self->helper( version => sub { return $VERSION });

  $self->helper(
    standard_date => sub {
      my ( $self, $day, $month, $year ) = @_;
      $day   = length $day   gt 1 ? $day   : "0$day";
      $month = length $month gt 1 ? $month : "0$month";
      return "$year-$month-$day";
    });


  $self->helper(
    lipsum => sub {
      my $lorem = <<'LOREM_IPSUM';

Ab quod qui beatae tempore dolore. Impedit quo ex quas non. Voluptas quasi consequuntur voluptatum ipsa beatae optio qui maxime. Corporis magnam non quia voluptas quidem soluta id. Recusandae consequatur illum sunt officia temporibus quis necessitatibus alias. Dolores itaque repellat omnis.

Culpa minima ut nam velit autem. Ut sed est itaque. Quo quia minima autem ut laborum sed impedit. Optio accusantium officia cum.

Aperiam nisi et aut qui expedita occaecati mollitia. Hic culpa quibusdam tempore voluptates expedita nulla ea sit. Quisquam ut quas et animi a aut id.

Voluptas necessitatibus dolorem porro adipisci quasi ut quae. Repellendus sed ad enim. Voluptas doloribus occaecati id. Ducimus aut quas dolor quibusdam et sed nam ea. Eum quas debitis veritatis voluptas rem numquam porro.

Voluptatum similique eos est facere quo. Similique et officia illo facilis ut similique minus. Cupiditate consequatur corporis minima officiis ipsam officiis. Nisi explicabo sit impedit recusandae nulla praesentium dolor quia. Consequatur repellat fugit tenetur praesentium. Sed laudantium eaque pariatur quis magnam occaecati.

LOREM_IPSUM
      return $lorem;
  });

  $self->helper(
    sketch => sub {
      my $number =  1 + int rand(12);
      $number = $number < 10 ? "0" . $number : $number;
      return '/img/scribbbles/scribbbles-' . $number . '.svg' ;
    });

  # Configure the application
  $self->secrets($config->{secrets});

  # Router
  my $r = $self->routes;

  $r->get('/a-propos')
          ->to('kiwi#about');
  $r->get('/nouvelles')
          ->to('kiwi#news');

  $r->get('/:year/:month')
    ->to( 'agenda#root',
          year => Time::Moment->now->year,
          month => Time::Moment->now->month);

  $r->get('/:year/:month/:title/:id')
          ->to('event#one');

  $r->get('/evenement/+/:day/:month/:year')
    ->to('event#add');

  $r->post('/evenement/creation')
    ->to('event#create');

  $r->get('/events/:year/:month')
    ->to('event#get_all_by_month');


}

=encoding utf8

=head1 NAME

Paris::Kiwi - DIY scene events calendar

=head1 DESCRIPTION

L<Paris::Kiwi|https://pariskiwi.org> is a wiki events agenda for Paris and it's
suburb. This is the second software version, the first one was based on
Mediawiki.

=head1 SYNOPSIS

  my $agenda = Paris::Kiwi::Agenda->new({
    current_year => $c->param('year'),
    current_month => $month
  });

=head1 HELPERS

They can be used in template by being called by their name but are globally accessible from everywhere with C<app->helper_name>

=head2 db

Give access to the db schema from everywhere.

=head2 standard_date

Turns idx dates, e.g. C<2020-4-1> used by some calendar or datetime toolkits to
proper dates e.g. C<2020-04-01>.

=head1 AUTHORS

Sébastien Feugère <sebastien@feugere.net>

=head1 LICENSE

Copyright (C) Sébastien Feugère

This library is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut



1;
