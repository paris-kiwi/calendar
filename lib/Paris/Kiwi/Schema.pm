use utf8;
package Paris::Kiwi::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-04-10 13:12:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fj1hIIsEA2kKd8JkTJQa4w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
