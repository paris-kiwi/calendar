package Paris::Kiwi::Event;
use Mojo::Base -base, -signatures;
use Role::Tiny::With;

with 'Paris::Kiwi::Role::Identifier';

has 'datetime';
# Ref. to a Paris::Venue
has 'venue';


1;
