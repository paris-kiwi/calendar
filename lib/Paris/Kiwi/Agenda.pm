package Paris::Kiwi::Agenda;
use Calendar::Simple;
use Data::Printer;
use List::MoreUtils qw( firstidx );
use Mojo::Base -base, -signatures;
use Mojo::Util 'encode';
use Role::Tiny::With;

with 'Paris::Kiwi::Role::DateTime';

# Those are constructors used on each page load
# They can be used as accessors too
# They are Integer, not the Str representation
has 'current_year';
has 'current_month';

has 'month' => sub( $self ) { $self->_build_month };
has 'start_year' => sub( $self ) {  $self->years->[0]  };

# TODO
# has 'events';

=head1

=head2 _build_month()

This generate a monthly calendar based on Calendar::Simple. If a month or a year
have been found in the request parameters, it will use them, otherwise it will set
them to january.

=cut
sub _build_month( $self ) {
  my @month = calendar(
    $self->current_month,
    $self->current_year || undef,
    1 );
  return \@month;
}

=head2 next_month_route()

Convert a textual month to an idx. Note that it return a route, not actually the 'next-month'

=cut
sub next_month_route( $self ) {
  # December, last index
  if ( $self->current_month == 12 ) {
    # Increment year and set month to january
    return '/' .
      $self->increment_year . '/' .
      # Not a Time::Moment index, this is a real array index
      @{ $self->months }[ 0 ];
  } else {
    return '/' .
      $self->current_year . '/' .
      @{ $self->months }[ $self->current_month ];
  }
}

# Managing the next year index
# Just build the year, don't modify it in the Agenda object
sub increment_year( $self ) {
  return $self->current_year + 1;
}

# Managing the next year index
# Just build the year, don't modify it in the Agenda object
sub decrement_year( $self ) {
  return $self->current_year - 1;
}

sub previous_month_route( $self ) {
  # January, first index
  if ( $self->current_month == 1 ) {
    # Decrement year and set month to december
    return '/' .
      $self->decrement_year . '/' .
      # Not a Time::Moment index, this is a real array index
      @{ $self->months }[ 11 ];
  } else {
    return '/' .
      $self->current_year . '/' .
      # -2 because this is not an index but a Time::Moment month
      @{ $self->months }[ $self->current_month - 2 ];
  }
}

sub previous_year_route( $self ) {
  return '/' .
    $self->decrement_year  . '/' .
    @{ $self->months }[ $self->current_month - 1];
}


=head1 next_year_route()

Note that it return a route, not actually the 'next-year'

=cut
sub next_year_route( $self ) {
  return '/' .
    $self->increment_year . '/' .
    # -1 because this is not an index but a Time::Moment month
    @{ $self->months}[ $self->current_month - 1 ];
}

1;
