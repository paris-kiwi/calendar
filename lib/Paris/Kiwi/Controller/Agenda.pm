package Paris::Kiwi::Controller::Agenda;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Paris::Kiwi::Agenda;
use Data::Printer;
use Role::Tiny::With;
with 'Paris::Kiwi::Role::DateTime';

sub root( $c ) {

  my $month = $c->normalize_month( $c->param('month'));

  my $agenda = Paris::Kiwi::Agenda->new({
    current_year => $c->param('year'),
    current_month => $month
  });

  $c->stash(

    # This is the actual initialization of everything
    # Not should but should stay on top
    agenda => $agenda->month,

    # Just an arrays with localized values
    days => $agenda->week,
    months => $agenda->months,
    years => $agenda->years,

    # Those are integers
    current_year => $agenda->current_year,
    current_month => $agenda->current_month,

    # Those are used by the navigation arrows
    previous_month =>  $agenda->previous_month_route,
    next_month =>  $agenda->next_month_route,
    previous_year => $agenda->previous_year_route,
    next_year =>  $agenda->next_year_route,
   );

  $c->render( page => 'Agenda');
}


1;
