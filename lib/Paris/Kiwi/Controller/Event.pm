package Paris::Kiwi::Controller::Event;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Mojo::Util 'slugify';
use Data::Printer;
use Role::Tiny::With;
use Time::Moment;

use Role::Tiny::With;
with 'Paris::Kiwi::Role::DateTime';

sub one ( $c ) {
  my $month = $c->param('month');
  my $year = $c->param('year');
  my $id = $c->param('id');
  my $event = $c->db->resultset('Event')->find( $id );

  $c->stash(
      id          => $event->id,
      title       => $event->title,
      description => $event->description,
      timestamp   => $event->timestamp,
      time_human  => $c->timestamp_to_localized_human( $event->timestamp ),
      start_time  => $c->timestamp_to_time( $event->timestamp ),
      url         => $c->url_for( '/' . join '/', $year, $month, slugify( $event->title ), $event->id )->to_abs
     );
  $c->render( page => 'Évènement' );
}

sub get_all_by_month( $c ) {
  my $year = $c->param('year');
  my $month = $c->param('month');
  my @to_front = ();
  my @events = $c->db->resultset('Event')->search({
      timestamp => { '-between' => [
          $c->month_start_epoch( $year, $month ),
          $c->month_end_epoch($year, $month),
         ]}
     });
  for (@events) {
      push @to_front, {
          id          => $_->id,
          title       => $_->title,
          description => $_->description,
          timestamp   => $_->timestamp,
          time_human  => $c->timestamp_to_human( $_->timestamp ),
          url         => join '/', slugify( $_->title ), $_->id
      };
  }
  # Turn the results to JSON
  $c->render( json => \@to_front );
}

sub add( $c ) {
  $c->render( page => 'Ajout');
}

sub create( $c ) {

  my $description = $c->param('description');
  my $title = $c->param('title');
  my $timestamp = $c->
          date_to_timestamp( $c->param('timestamp') .'T'. $c->param('start'));

  my $start = $c->param('start');
  p $start;

  my $tm = Time::Moment->from_epoch( $timestamp );

  my $new_event =  $c->db->resultset('Event')->new({
      title => $title,
      description => $description,
      timestamp =>  $timestamp
  })->insert;

  $c->flash(
      msg => 'Ajouté',
      status => 'info'
     );
  $c->redirect_to( '/' . join '/' , $tm->year, $tm->month );
}



1;
