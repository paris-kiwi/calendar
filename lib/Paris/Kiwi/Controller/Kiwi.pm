package Paris::Kiwi::Controller::Kiwi;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub news( $c ) {
  $c->render( page => 'Nouvelles' );
}

sub about( $c ) {
    $c->render( page => 'À propos' );
}


1;
