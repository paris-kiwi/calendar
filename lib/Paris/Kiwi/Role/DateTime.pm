package Paris::Kiwi::Role::DateTime;
use Data::Printer;
use Time::Moment;
use Mojo::Base -role, -signatures;
use List::MoreUtils qw( firstidx );


# Those are some constants

has 'years' => sub( $self ) { $self->_years };
has 'months' => sub( $self ) { $self->_months };
has 'week' => sub( $self ) { $self->_week_days };

sub _years( $self ) {
    my $current = Time::Moment->now->year;
    my @years_range = ( 2012..$current + 1 );
    return \@years_range;
}

sub _months {
    my @months = qw( janvier février mars avril
                     mai juin juillet août
                     septembre octobre novembre décembre);
    return \@months;
}

sub _week_days {
    my @days = qw( Lundi Mardi Mercredi Jeudi
                   Vendredi Samedi Dimanche );
    return \@days;
}

=head1 date_to_time()

Takes a date of the 2020-4-1 in argument and convert it to an I<epoch> timestamp.

=cut
sub date_to_timestamp ( $self, $str ) {
  $str =~ /(\d+)-(\d+)-(\d+)T(\d{2}):(\d{2})/;
  my $tm = Time::Moment->new(
    year => $1,
    month => $2,
    day => $3,
    hour => $4,
    minute => $5);
  return $tm->epoch;
}

sub timestamp_to_time ( $self, $timestamp ) {
    return Time::Moment->from_epoch( $timestamp )->hour . ':' .
            Time::Moment->from_epoch( $timestamp )->minute;
}


sub timestamp_to_human ( $self, $timestamp ) {
  return Time::Moment->from_epoch( $timestamp )->to_string;
}

sub timestamp_to_localized_human ( $self, $timestamp ) {
    my $tm = Time::Moment->from_epoch( $timestamp );
    return
            @{ $self->week }[ $tm->day_of_week - 1 ] .' '.
            $tm->day_of_month .' '.
            ucfirst @{ $self->months }[ $tm->month - 1 ] .' '.
            $tm->year;
}


sub month_start_epoch ( $self, $year, $month ) {
  my $tm = Time::Moment->new(
    year => $year,
    month => $month,
    day => 1 );
  return $tm->epoch;
}

sub month_end_epoch ( $self, $year, $month ) {
  my $tm = Time::Moment->new(
    year => $year,
    month => $month,
    hour => 23,
    minute => 59,
    second => 59 )->at_last_day_of_month;
  return $tm->epoch;
}

=head2 normalize_month

Normalize month by converting the month name to an idx (if necessary) It
constructs (sometimes, if matching a word) the returned value by comparing an
index with the month real names so it always return -1

The month_str is a Time::Moment id (starts at 1, not 0)

=cut
sub normalize_month ( $self, $month_str ) {
    # We only convert the month name to an index if we got a word
    if ( $month_str =~ /[^0-9]+/ ) {
        my $idx = firstidx { $_ eq $month_str } @{ $self->months };
        if ( $idx == -1 ) {
            $idx = 0;
        }
        return $idx + 1;
    }
    return $month_str;
}



1;
