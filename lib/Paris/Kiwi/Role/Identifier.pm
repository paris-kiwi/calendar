package Paris::Kiwi::Role::Identifier;
use Mojo::Base -role, -signatures;

has 'id' => sub { int rand 100 };

1;
