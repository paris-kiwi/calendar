use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

isa_ok $t->app->db, 'DBIx::Class::Schema';
isa_ok $t->app->db->resultset('Event'), 'DBIx::Class';

my @events = $t->app->db->resultset('Event')->search({});

for ( @events ) {
    isa_ok  $_, 'DBIx::Class::Core';
    ok $_->id;
    ok $_->timestamp;
    ok $_->title;
    ok $_->description;
}

done_testing();
