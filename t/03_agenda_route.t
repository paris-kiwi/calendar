use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;
use Role::Tiny;
use Paris::Kiwi::Agenda;
use Time::Moment;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $time = Time::Moment->new(
    year       => 2015,
    month      => 1);

my $agenda = Paris::Kiwi::Agenda->new(
    current_year  => $time->year,
    current_month => $time->month
   );

is $agenda->next_month_route, "/2015/février", "Building next month route";
is $agenda->next_year_route, "/2016/janvier", "Building next year route";

is $agenda->next_month_route, "/2015/février", "Building next month route";

# Tester le passage d'année en rajoutant un mois à partir de décembre


done_testing();
