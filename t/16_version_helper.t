use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

like $t->app->version, qr/^[0-9]+\.[0-9]{2}$/, 'Version number helper works';


done_testing();
