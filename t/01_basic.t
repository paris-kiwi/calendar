use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;
use Role::Tiny;
use Time::Moment;
use Paris::Kiwi::Agenda;
use Paris::Venue;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');
$t->get_ok('/')->status_is(200)->content_like(qr/Paris::Kiwi/i);

my $agenda = Paris::Kiwi::Agenda->new;

is $agenda->start_year, 2012,
        'the agenda start year is set and correct';

my @supposed_years = ( $agenda->start_year .. Time::Moment->now->year + 1 );

ok $agenda->years ~~ @supposed_years,
        'the agenda years array is correctly generated';

my $venue = Paris::Venue->new({ address => '26 rue de Neuchatel' });

ok Role::Tiny::does_role( $venue, 'Paris::Kiwi::Role::Identifier');
ok Role::Tiny::does_role( $venue, 'Paris::Kiwi::Role::Website');

# TODO BUG there is an obvious error, the current month should be an index, not
# a string. When retrieved from the select it should be converted to an index


done_testing();
