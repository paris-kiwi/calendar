use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';


my $t = Test::Mojo->new('Paris::Kiwi');

$t->get_ok( "/" )
        ->element_exists('body > footer')
        ->element_exists('body > div > section > table');

$t->get_ok( "/2017/février" )
        ->element_count_is('table tr td form input', 28);

$t->get_ok( "/2019/novembre" )
        ->element_exists(
            'table tr td[data-date=2019-11-05] form[action=/evenement/+/5/11/2019]');

$t->get_ok( "/2020/avril" )
        ->element_exists('table[data-month=4]')
        ->element_exists('table[data-year=2020]')
        ->element_exists('table[class=table table--zebra]');

$t->get_ok( "/2016/mai" )
        ->element_exists('form[id=previous-month][action=/2016/avril]')
        ->element_exists('form[id=select-month][action=/generic]')
        ->element_exists('form[id=next-month][action=/2016/juin]');

$t->get_ok( "/2013/décembre" )
        ->element_exists('form[id=previous-month][action=/2013/novembre]')
        ->element_exists('form[id=select-month][action=/generic]')
        ->element_exists('form[id=next-month][action=/2014/janvier]');


$t->get_ok( "/2017/janvier" )
  ->element_exists('form[id=previous-month][action=/2016/d%C3%A9cembre]')
  ->element_exists('form[id=select-month][action=/generic]')
  ->element_exists('form[id=next-month][action=/2017/f%C3%A9vrier]');


$t->get_ok( "/2013/décembre" )
        ->element_exists('form[id=previous-year][action=/2012/d%C3%A9cembre]')
        ->element_exists('form[id=select-year][action=/generic]')
        ->element_exists('form[id=next-year][action=/2014/d%C3%A9cembre]');


my $tt = $t->get_ok( "/2017/mars" );
for ( 1..31 ) {
    # In case the day is 1, not 01
    s/^(\d{1})$/0$1/;
    $tt->element_exists("table tr td[class=day][data-date=2017-03-$_]");
}


done_testing();
