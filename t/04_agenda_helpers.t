use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;
use Paris::Kiwi::Agenda;
use Time::Moment;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

is $t->app->standard_date(( 15, 3, 2016 )), '2016-03-15';
is $t->app->standard_date(( 1, 3, 2016 )), '2016-03-01';
# Non-sense date
is $t->app->standard_date(( 45, 67, 9999 )), '9999-67-45';
# It works :D !

done_testing();
