use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

$t->get_ok('/events/2020/4')
  ->status_is(200)
        ->json_has('/1/description')
        ->json_has('/1/title')
        ->json_has('/1/timestamp')
        ->json_has('/1/time_human')
        ->json_is('/1/description', 'roque andeux rolles')
        ->json_is('/1/time_human', '2020-04-16T00:00:00Z')
        ->json_is('/1/timestamp', 1586995200)
        ->json_is('/1/title', 'paris kiwi fest');


done_testing();
