use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

is $t->app->standard_date( 1, 3, 2020), '2020-03-01';
is $t->app->standard_date( 10, 3, 2020), '2020-03-10';
is $t->app->standard_date( 21, 12, 2017), '2017-12-21';

done_testing();
