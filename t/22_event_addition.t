use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

$t->get_ok('/evenement/+/2/4/2020')
        ->status_is(200)
        ->element_exists('form[action=/evenement/creation]')
        ->element_exists('form input[name=title][type=text]')
        ->element_exists('form textarea[name=description]')
        ->element_exists('form input[name=start][type=time]')
        ->element_exists('form input[name=timestamp][type=hidden][value=2020-4-2]')
        ->element_exists('form input[value=Ok][type=submit]')
        ;


done_testing();
