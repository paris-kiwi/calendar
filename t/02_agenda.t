use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;
use Role::Tiny;
use Paris::Kiwi::Agenda;
use Time::Moment;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $time = Time::Moment->new(
  year       => 2012,
  month      => 12);

my $agenda = Paris::Kiwi::Agenda->new(
  current_year  => $time->year,
  current_month => $time->month
 );

isa_ok $agenda, 'Mojo::Base';
isa_ok $agenda, 'Paris::Kiwi::Agenda';

ok Role::Tiny::does_role( $agenda, 'Paris::Kiwi::Role::DateTime' ), 'Agenda does DateTime role';

my @attributes = qw/ current_month current_year month start_year /;
my @inherited = qw/ years months week /;
my @methods = qw/ increment_year decrement_year next_year_route normalize_month /;

can_ok $agenda, @attributes;
can_ok $agenda, @inherited;
can_ok $agenda, @methods;

cmp_ok $agenda->current_month, '==', 12, 'Month is december';
like $agenda->current_month, qr/\d{2}/, 'Month is a number';

my $time2 = Time::Moment->new( year => 2015, month => 1);

my $agenda2 = Paris::Kiwi::Agenda->new(
  current_year  => $time2->year,
  current_month => $time2->month
 );

cmp_ok $agenda2->current_month, '==', 1, 'Month is january';
like $agenda2->current_month, qr/\d{1}/, 'Month is a number';
is $agenda2->current_year, 2015, 'Year is 2015';
like $agenda2->current_year, qr/\d{4}/, 'Year is a number';

is ref $agenda->month, 'ARRAY', '$agenda->month contains an ARRAY';
is ref $agenda->month->[0], 'ARRAY', '$agenda->month is an ARRAY of ARRAY';

is_deeply $agenda->month->[0],
  [undef,undef,undef,undef,undef,1,2], 'Checking a week of $agenda->month';
is_deeply $agenda->month->[1], [3..9],
  'Checking a week of $agenda->month';
is_deeply $agenda->month->[2], [10..16],
  'Checking a week of $agenda->month';
is_deeply $agenda->month->[2], [10..16],
  'Checking it again so we are sure it won\'t mutate';
is_deeply $agenda->month->[3], [17..23],
  'Checking a week of $agenda->month';
is_deeply $agenda->month->[4], [24..30],
  'Checking a week of $agenda->month';
is_deeply $agenda->month->[5],
  [31,undef,undef,undef,undef,undef,undef],
  'Checking a week of $agenda->month';

is $agenda->start_year, 2012, 'Start year is correct';

done_testing();
