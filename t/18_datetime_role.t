use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Time::Moment;
use Paris::Kiwi::Agenda;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $time = Time::Moment->new(
    year       => 2018,
    month      => 07);

my $agenda = Paris::Kiwi::Agenda->new(
    current_year  => $time->year,
    current_month => $time->month
   );

isa_ok $agenda, 'Mojo::Base';
isa_ok $agenda, 'Paris::Kiwi::Agenda';

ok Role::Tiny::does_role( $agenda, 'Paris::Kiwi::Role::DateTime' ), 'Agenda does DateTime role';

my @inherited = qw/ years
                    months
                    week /;
my @methods = qw/ date_to_timestamp
                  timestamp_to_human
                  month_start_epoch
                  month_end_epoch
                  normalize_month /;

can_ok $agenda, @inherited;
can_ok $agenda, @methods;

for ( 1 .. 12 ) {
    is $agenda->normalize_month($_), $_,
            'normalize_month returns an integer when passed an Int ' .
            $_;
}

while ( my ( $i, $month ) = each( @{ $agenda->months } )) {
    is $agenda->normalize_month( $month ), $i + 1,
            'normalize_month returns an integer index when passed a Str ' .
            @{ $agenda->months }[$i];
}

done_testing();
