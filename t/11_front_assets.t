use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my @assets = qw/ async_events.js
                 jquery.min.js
                 kiki.txt
                 knacss.min.css
                 styles.css /;

my $t = Test::Mojo->new('Paris::Kiwi');

for ( @assets ) {
  $t->get_ok( "/$_" )
    ->status_is(200);
}

done_testing();
