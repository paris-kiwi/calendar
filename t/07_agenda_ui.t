use Mojo::Base -strict;

use open ':std', ':encoding(utf8)';
use Test::More;
use Test::Mojo;
use Data::Printer;
use Mojo::Util 'url_unescape';
use Role::Tiny;
use Paris::Kiwi::Agenda;
use Time::Moment;

no if ( $] >= 5.018 ), 'warnings' => 'experimental';

my $t = Test::Mojo->new('Paris::Kiwi');

$t->get_ok('/2020/février')
  ->status_is(200)
  ->element_count_is('td.day', 29, 'Calendar got the right number of cells days');

$t->get_ok('/2020/février')
  ->attr_is('form#previous-month', 'action', '/2020/janvier');

$t->get_ok('/2020/mars')
  ->status_is(200)
  ->element_count_is('td.day', 31, 'Calendar got the right number of cells days');

$t->get_ok('/2020/mars')
  ->attr_is('form#previous-month', 'action', '/2020/f%C3%A9vrier');



# Check the page contains the month name
# is $agenda2->current_month, $time2->strftime('%B');


done_testing();
