CREATE TABLE IF NOT EXISTS "event"
(
	id integer not null
		primary key,
	timestamp integer not null,
	description text,
	title text not null
);
CREATE UNIQUE INDEX event_id_uindex
	on event (id);
