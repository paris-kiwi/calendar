PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "event"
(
	id integer not null
		primary key,
	timestamp integer not null,
	description text,
	title text not null
);
INSERT INTO event VALUES(1,1585699200,'la police','nik');
INSERT INTO event VALUES(6,1579219200,'popo pipi','gaaaaa');
INSERT INTO event VALUES(7,1580256000,'Reverend Beatman','Trash Punk');
INSERT INTO event VALUES(8,1586995200,'roque andeux rolles','paris kiwi fest');
INSERT INTO event VALUES(9,1588204800,'pogo','paris kiwi fest');
INSERT INTO event VALUES(10,1587686400,'chaton','paris kiwi fest');
INSERT INTO event VALUES(11,1578009600,'un bon chat','paris kiwi fest');
INSERT INTO event VALUES(12,1577923200,'chat','paris kiwi fest');
INSERT INTO event VALUES(13,1586995200,'attrappe des souris','coucou les keupons');
INSERT INTO event VALUES(14,1586995200,'jkljkljkjkljkjklk','ggg');
INSERT INTO event VALUES(15,1587081600,'C''est un groupe de youpis','Effello @ Paris Arena Bercy');
INSERT INTO event VALUES(16,1585699200,'éèà,c','évènement avec accent');
INSERT INTO event VALUES(17,1587513600,'yoooo','coucou les keupons');
CREATE UNIQUE INDEX event_id_uindex
	on event (id);
COMMIT;
